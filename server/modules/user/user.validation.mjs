import Joi from "@hapi/joi";

export const CREATE_USER = "CREATE_USER";
export const UPDATE_USER = "UPDATE_USER";

export const validate_user = async (payload, type = UPDATE_USER) => {
  let first_name = Joi.string().alphanum().min(3).max(30);
  let last_name = Joi.string().alphanum().min(3).max(30);
  let email = Joi.string().email({ minDomainSegments: 2 });
  // let phone = Joi.string().regex(/^\d{3}-\d{3}-\d{4}$/);

  if (type === CREATE_USER) {
    first_name = first_name.required();
    last_name = last_name.required();
    email = email.required();
    // phone = phone.required();
  }

  const schema = Joi.object().keys({ first_name, last_name, email });

  return await schema.validateAsync(payload);
};
