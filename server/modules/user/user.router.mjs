/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import express from "express";
import log from "@ajar/marker";
import connection from "../../db/mysql.connection.mjs";
import { CREATE_USER, UPDATE_USER, validate_user } from "./user.validation.mjs";

const router = express.Router();

const TABLE = "MOCK_DATA";

// Parse json req.body on post routes
router.use(express.json());

// CREATE NEW USER
router.post(
  "/",
  raw(async (req, res) => {
    await validate_user(req.body, CREATE_USER);

    const [[doesExist]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE email="${req.body.email}";`
    );
    if (doesExist)
      throw new Error("A user already exists with this email address");

    const sqlQuery = `INSERT INTO ${TABLE} SET ?`;

    const [result] = await connection.query(sqlQuery, req.body);
    const ok = { status: 200, message: `User Created successfully` };
    const fail = { status: 404, message: `Error in creating user ` };
    const { status, message } = result.affectedRows ? ok : fail;

    const [[created_user]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE email="${req.body.email}";`
    );

    return res.status(status).json({
      message,
      auth: true,
      user: created_user,
    });
  })
);

// GET ALL USERS
router.get(
  "/",
  raw(async (req, res) => {
    const [getResponse] = await connection.query(`SELECT * FROM ${TABLE};`);
    res.status(200).json(getResponse);
  })
);

// GET PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req, res) => {
    let { page = 0, items = 10 } = req.params;
    const [getResponse] = await connection.query(
      `SELECT * FROM ${TABLE} LIMIT ${items} OFFSET ${page};`
    );

    res.status(200).json(getResponse);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req, res) => {
    const [[getResponse]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    res.status(200).json(getResponse);
  })
);

// UPDATES A WHOLE USER
router.put(
  "/:id",
  raw(async (req, res) => {
    await validate_user(req.body, UPDATE_USER);

    const [[getResponse]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );

    const [queryResponse] = await connection.query(`UPDATE ${TABLE}
      SET first_name = "${req.body.first_name}", last_name = "${req.body.last_name}", email = "${req.body.email}"
      WHERE id = ${req.params.id};`);

    if (queryResponse.affectedRows > 0) res.status(200).json(getResponse);
  })
);

// UPDATE SOME PROPERTIES IN USER
router.patch(
  "/:id",
  raw(async (req, res) => {
    await validate_user(req.body, UPDATE_USER);

    const [[getResponse]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );

    let patchQuery = `UPDATE ${TABLE} SET `;
    Object.keys(req.body).map(
      (property) => (patchQuery += `${property} = "${req.body[property]}", `)
    );
    patchQuery = patchQuery.slice(0, patchQuery.length - 2);
    patchQuery += ` WHERE id=${req.params.id};`;

    const [queryResponse] = await connection.query(patchQuery);

    if (queryResponse.affectedRows > 0) res.status(200).json(getResponse);
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req, res) => {
    const [[getResponse]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const [queryResponse] = await connection.query(`DELETE FROM ${TABLE}
      WHERE id = ${req.params.id};`);
    if (queryResponse.affectedRows > 0) res.status(200).json(getResponse);
  })
);

export default router;
