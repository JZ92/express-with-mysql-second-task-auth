import bcrypt from "bcryptjs";
import express from "express";
import log from "@ajar/marker";
import raw from "../../middleware/route.async.wrapper.mjs";
import connection from "../../db/mysql.connection.mjs";
import {
  verify_token,
  false_response,
  tokenize,
} from "../../middleware/auth.middleware.mjs";

const router = express.Router();

router.use(express.json());

const TABLE = "MOCK_DATA";

router.post(
  "/register",
  raw(async (req, res) => {
    log.obj(req.body, "register, req.body:");

    const [[doesExist]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE email="${req.body.email}";`
    );
    if (doesExist)
      throw new Error("A user already exists with this email address");

    const salt = await bcrypt.genSalt(10);

    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const sqlQuery = `INSERT INTO ${TABLE} SET ?`;
    const reqBodyWithHashedPass = {
      ...req.body,
      password: hashedPassword,
    };
    const [result] = await connection.query(sqlQuery, reqBodyWithHashedPass);
    const ok = { status: 200, message: `User Created successfully` };
    const fail = { status: 404, message: `Error in creating user ` };
    const { status, message } = result.affectedRows ? ok : fail;

    const [[createdUser]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE email="${req.body.email}"`
    );

    // Create a token
    const token = tokenize(createdUser.id);

    return res.status(status).json({
      message,
      auth: true,
      token,
      user: createdUser,
    });
  })
);

router.post(
  "/login",
  raw(async (req, res) => {
    // Extract from req.body the credentials the user entered
    const { email, password } = req.body;

    // Look for the user in db by email
    const [[user]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE email="${email}";`
    );

    // If no user were found
    if (!user)
      return res
        .status(401)
        .json({ ...false_response, message: "Wrong email or password" });

    // check if the password is valid
    const password_is_valid = await bcrypt.compare(password, user.password);

    if (!password_is_valid)
      return res
        .status(401)
        .json({ ...false_response, message: "Wrong email or password" });

    // If user is found and password is valid - create a fresh new token
    const token = tokenize(user.id);

    // return the information including token as JSON
    return res.status(200).json({
      auth: true,
      token,
    });
  })
);

router.get(
  "/logout",
  raw(async (req, res) => {
    return res.status(200).json(false_response);
  })
);

router.get(
  "/me",
  verify_token,
  raw(async (req, res) => {
    const [[user]] = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id="${req.user_id}";`
    );
    if (!user) return res.status(404).json({ message: "No user found." });
    res.status(200).json(user);
  })
);

export default router;
