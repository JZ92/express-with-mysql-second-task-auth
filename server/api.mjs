import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import user_router from "./modules/user/user.router.mjs";
import auth_router from "./modules/auth/auth.router.mjs";
import { error_handler, not_found } from "./middleware/errors.handler.mjs";

const { PORT, HOST } = process.env;

const app = express();

// Middlewares
app.use(cors());
app.use(morgan("dev"));

// Routing
app.use("/api/users", user_router);
app.use("/api/auth", auth_router);

// Central error handling
app.use(error_handler);

// When no routes were matched
app.use("*", not_found);

// Start the Express api server
(async () => {
  await app.listen(PORT, HOST);
  log.magenta(`api is live on`, ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
})().catch(log.error);
